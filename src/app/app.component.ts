import { FormControl, FormGroup } from '@angular/forms';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  form = new FormGroup({
    isSubscribe: new FormControl(),
    radio: new FormControl(),
    select: new FormControl(),
    input: new FormControl()
  });
  selectOptions = [ 'value1', 'value2', 'value3'];
  title = 'app';
  isChecked = true;
  constructor() {
    this.form.get('select').setValue( this.selectOptions[2] )
    this.form.get('select').disable();
  }
  onChange() {
    console.log(this.form.value);
  }
}
