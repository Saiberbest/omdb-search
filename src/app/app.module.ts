import { RouterModule } from '@angular/router';
import { OMDbService } from './services/OMDb.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import {
  MatCheckboxModule,
  MatRadioModule,
  MatSelectModule,
  MatFormFieldModule,
  MatInputModule,
  MatButtonModule,
  MatListModule,
  MatCardModule,
  MatAutocompleteModule
 } from '@angular/material';
import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SearchFilmsComponent } from './search-films/search-films.component';
import { AppErrorHandler } from './common/app-error-handler';
import { HttpModule } from '@angular/http';
import { ShowFilmComponent } from './show-film/show-film.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { FilmsComponent } from './films/films.component';
import { FilmsListService } from './services/films-list.service';


@NgModule({
  declarations: [
    AppComponent,
    SearchFilmsComponent,
    ShowFilmComponent,
    NavBarComponent,
    FilmsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatListModule,
    MatCardModule,
    MatRadioModule,
    MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule.forRoot([
      {
        path: '',
        redirectTo: '/search',
        pathMatch: 'full'
      },
      {
        path: 'films/:id',
        component: ShowFilmComponent
      },
      {
        path: 'films-list',
        component: FilmsComponent
      },
      {
        path: 'search',
        component: SearchFilmsComponent
      }
    ])
  ],
  providers: [
    OMDbService,
    FilmsListService
    // { provide: ErrorHandler, useClass: AppErrorHandler }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
