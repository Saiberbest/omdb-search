import { Component, OnInit } from '@angular/core';
import { FilmsListService } from '../services/films-list.service';
import { FormControl } from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import {startWith} from 'rxjs/operators/startWith';
import {map} from 'rxjs/operators/map';

@Component({
  selector: 'app-films',
  templateUrl: './films.component.html',
  styleUrls: ['./films.component.css']
})
export class FilmsComponent implements OnInit {
  filmCtrl: FormControl;
  filmsList: any[];

  filteredFilms: Observable<any[]>;

  constructor(private filmsListService: FilmsListService) {
    this.filmCtrl = new FormControl();
    this.filteredFilms = this.filmCtrl.valueChanges
      .pipe(
        startWith(''),
        map(film => film ? this.filterFilms(film) : this.filmsList.slice())
      );
  }


  filterFilms(title: string) {
    return this.filmsList.filter(film =>
      film.title.toLowerCase().indexOf(title.toLowerCase()) === 0);
  }

  ngOnInit() {
    this.filmsList = this.filmsListService.getList();
  }

}
