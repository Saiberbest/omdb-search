import { FilmsListService } from '../services/films-list.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MovieType, SearchParams } from '../types/search';
import { OMDbService } from '../services/OMDb.service';

@Component({
  selector: 'search-films',
  templateUrl: './search-films.component.html',
  styleUrls: ['./search-films.component.css']
})
export class SearchFilmsComponent implements OnInit {

  types = [
    { name: 'Film', value: MovieType.movie },
    { name: 'Episode', value: MovieType.episode },
    { name: 'Series', value: MovieType.series }
  ];

  films: any[];
  message = '';

  form = new FormGroup({
    title: new FormControl(),
    type: new FormControl(),
    year: new FormControl()
  });

  constructor(private filmService: OMDbService, private filmsListService: FilmsListService) { }

  ngOnInit() {
  }

  search() {
    const value = this.form.value;
    this.message = 'Searching films...';
    const searchParams = new SearchParams(value.title, value.year, value.type);
    this.filmService.search(searchParams).subscribe(search => {
      if (search.Error) {
        this.message = search.Error;
        this.films = [];
      }else {
        this.message = '';
        this.films = search.Search;
      }
    });
  }

  addFilm(id: string, title: string) {
    this.filmsListService.addFilm(id, title);
  }

}
