import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import { AppError } from '../common/app-error';
import { NotFoundError } from '../common/not-found-error';
import { BadInput } from '../common/bad-input';
import { SearchParams } from '../types/search';

@Injectable()
export class OMDbService {

  url = 'http://www.omdbapi.com/?apikey=9c683efe&';

  constructor(private http: Http) { }

  search(searchParams: SearchParams) {
    return this.http.get(this.url + searchParams.toString() )
    .map(response => response.json() )
    .catch( this.handleError );
  }

  getFilmById(id: string) {
    console.log(id);
    return this.http.get(this.url + 'i=' + id + '&plot=full' )
    .map(response => response.json() )
    .catch( this.handleError );
  }

  private handleError(error: Response) {
    switch (error.status) {
      case 404:
        return Observable.throw(new NotFoundError());
      case 400:
        return Observable.throw(new BadInput(error.json()));
      default:
        return Observable.throw(new AppError(error));
    }
  }
}
