import { TestBed, inject } from '@angular/core/testing';

import { FilmsListService } from './films-list.service';

describe('FilmsListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FilmsListService]
    });
  });

  it('should be created', inject([FilmsListService], (service: FilmsListService) => {
    expect(service).toBeTruthy();
  }));
});
