import { Injectable } from '@angular/core';



@Injectable()
export class FilmsListService {

  private filmsList: any[] = [];

  constructor() {
    this.getList();
  }

  addFilm(id: string, title: string) {
    this.filmsList.push({id, title});
    localStorage.setItem('filmsList', JSON.stringify(this.filmsList));
  }

  getList() {
    const localFilmsList = JSON.parse(localStorage.getItem('filmsList'));
    if (localFilmsList) {
      this.filmsList = localFilmsList;
    }
    return this.filmsList;
  }



}
