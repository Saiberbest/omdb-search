import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OMDbService } from '../services/OMDb.service';

@Component({
  selector: 'show-film',
  templateUrl: './show-film.component.html',
  styleUrls: ['./show-film.component.css']
})
export class ShowFilmComponent implements OnInit {

  id: string;
  film: any;

  constructor(
    private route: ActivatedRoute,
    private filmService: OMDbService
  ) {
  }

  ngOnInit() {
    this.route.paramMap.map(params => {
      const id = params.get('id');
      this.id = id;
      return id;
    }).subscribe( id => {
      this.filmService.getFilmById(id).subscribe(film => {
        this.film = film;
      });
    });
  }

}
