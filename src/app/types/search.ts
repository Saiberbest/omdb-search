export enum MovieType {
  movie = 'movie',
  series = 'series',
  episode = 'episode'
}

export class SearchParams {

  constructor (
    private s: string,
    private y?: number,
    private type?: MovieType
    ) {
  }

  toString() {
    const s = '&s=' + this.s;
    const type = this.type ? ('&type=' + this.type) : '';
    const y = this.type ? ('&y=' + this.y) : '';
    return s + y + type;
  }
}
